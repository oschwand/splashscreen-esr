function splash() {
    var splash = document.createElement("div");
    splash.setAttribute("id", "splash");

    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", "https://su-agenda-militant.gitlab.io/splashscreen-esr/tract.html");

    var close = document.createElement("span");
    close.setAttribute("id", "close");
    var x = document.createTextNode("X");
    close.appendChild(x);
    close.onclick = function () {splash.style.display = "none"};

    splash.appendChild(iframe);
    splash.appendChild(close);
    document.body.appendChild(splash);
}
window.onload = splash;

