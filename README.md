Écran de grève pour vos sites web !

[Démo](https://oschwand.gitlab.io/splashscreen-esr/demo.html)

# Site statique

Ajoutez le code suivant dans l'entête de votre page:

~~~
<link href="https://oschwand.gitlab.io/splashscreen-esr/splash.css" rel="stylesheet">
<script src="https://oschwand.gitlab.io/splashscreen-esr/splash.js"></script>
~~~

# Autres sites

euh

